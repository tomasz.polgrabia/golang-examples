package main

import (
  "fmt"
  "math/rand"
)

func swap(x, y string) (string, string) {
  return y, x;
}

func add(x int, y int) int {
  return x + y;
}

func main() {
  fmt.Println("My favourite number is", rand.Intn(10))
  fmt.Println("Sum is", add(4, 5))
  a, b := swap("Hello", "World")
  fmt.Println("A: ", a, "B: ", b)
}
